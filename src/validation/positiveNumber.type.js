const { isNumber } = require('./types-validation');

class PositiveNumber {
    value = null;

    constructor(value) {
        let _value = Number(value);

        if(value !== null && isNumber(_value) && _value > 0){
            this.value = _value;
            Object.freeze(this);
            return;
        }

        throw new TypeError('Invalid Positive number');
    }
}

module.exports = PositiveNumber;
