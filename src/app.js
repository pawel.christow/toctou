'use strict';

// requirements
const express = require('express');

const PositiveNumber = require( './validation/positiveNumber.type');

// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();

// configurations
app.use(express.json());

// routes
// health check
app.get('/status', (req, res) => { res.status(200).end(); });
app.head('/status', (req, res) => { res.status(200).end(); });

// Send 
app.get('/', (req, res) => {
    var balance = 1000;
    let amount = 0;
    if(req.query && req.query.amount){
        amount = req.query.amount;
    } else {
        res.status(400).end('Amount is required')
    }
    var { balance, transfered } = transfer(balance, amount);

    if (balance !== undefined || transfered !== undefined) { 
        res.status(200).end('Successfully transfered: ' + transfered + '. Your balance: ' + balance);
    } else {
        res.status(400).end('Insufficient funds. Your balance: ' + balance);
    }
});

// Transfer amount service
var transfer = (balance, amount) => {
    let transfered = 0;
    let amountCopy;

    try {
        amountCopy = new PositiveNumber(amount).value;
    } catch (err) {
        console.log(err);
        return { undefined, undefined };
    }

    if (amountCopy <= balance) {
        balance = balance - amountCopy;
        transfered = transfered + amountCopy;
        return { balance, transfered };
    } else
        return { undefined, undefined };
};

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}
// CTRL+c to come to action
process.on('SIGINT', function() {
    process.exit();
});

module.exports = { app, transfer };
